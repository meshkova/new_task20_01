package ru.mea.max;

public class Max {
    public static void main(String[] args) {
        int[][] twoDimArray = new int[7][4];

        for (int i = 0; i < twoDimArray.length; i++) {
            for (int j = 0; j < twoDimArray[0].length; j++) {
                twoDimArray[i][j] = ((int) (Math.random() * 11) - 5);


                System.out.print(" " + twoDimArray[i][j] + " ");

            }
            System.out.println();

        }
        System.out.println(maxs(twoDimArray));
    }

    private static int maxs(int[][] twoDimArray) {
        int index = 0;
        int max = 0;

        if (twoDimArray != null) {
            for (int i = 0; i < twoDimArray.length; i++) {
                int num = 1;

                for (int j = 0; j < twoDimArray[0].length; j++) {
                    num *= Math.abs(twoDimArray[i][j]);
                }
                if (num > max) {
                    index = i;
                    max = num;
                }
            }
        }
        return index;

    }
}
